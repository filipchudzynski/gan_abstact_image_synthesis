import sys
from PIL import Image

images = [Image.open(x) for x in [ 'epoch396_0.png', 'epoch182_0.png','real_image_0.png','epoch175_0.png','epoch870_0.png']]
widths, heights = zip(*(i.size for i in images))

total_width = sum(widths[:3])
max_height = sum(heights[:3])

new_im = Image.new('RGB', (total_width, max_height))

x_offset = 0
#for im in images:
#  new_im.paste(im, (x_offset,0))
#  x_offset += im.size[0]

new_im.paste(images[0],(widths[0],0))
new_im.paste(images[1],(0,heights[0]))
new_im.paste(images[2],(widths[0],heights[0]))
new_im.paste(images[3],(2*widths[0],heights[0]))
new_im.paste(images[4],(widths[0],2*heights[0]))


new_im.save('test.jpg')

from PIL import Image as im
for i in range(50,51):
    img = im.open(str(i) + ".jpg")
    width, height = img.size
    area = ((width-height)/2, 0, height+(width-height)/2, height)
    # area = ((width-height)/2,0,height,height)
    cropped_img = img.crop(area)
    foo = cropped_img.resize((64,64),im.ANTIALIAS)
    foo.save("croped_reduced/"+str(i)+".jpg",quality=100)
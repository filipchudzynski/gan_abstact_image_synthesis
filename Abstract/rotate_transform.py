from PIL import Image as im
to_save=[]
for i in range(1,51):
    img = im.open("croped_reduced/" + str(i) + ".jpg")
    # area = ((width-height)/2,0,height,height)
    img90 = img.rotate(90)
    img180 = img.rotate(180)
    img270 = img.rotate(270)
    imgs = [img,img90,img180,img270]
    
    for rotated in imgs:
        to_save.append(rotated)
        to_save.append(rotated.transpose(im.FLIP_LEFT_RIGHT))
        to_save.append(rotated.transpose(im.FLIP_TOP_BOTTOM))



for ind,img in enumerate(to_save):

    img.save("rotated_transformed/"+str(ind)+".jpg",quality=100)